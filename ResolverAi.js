/**
 * @author mati@we.ee
 * @type ResolverSimple
 */
class ResolverAi {
    execute(row) {
        let parts = row.split(' ');
        if (parts.length < 2 || parts[1] !== 'auto') {
            return;
        }        
        return new RowAi(parts[0], 'int', true);
    }
}
