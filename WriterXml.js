/**
 * @author mati@we.ee
 * @type WriterXml
 */
class WriterXml extends Writer {

    write(rows, className) {
        var tn = className;
        if (tn.indexOf('\\') >= 0) {
            let parts = tn
                    .replace(/\\?([A-Z]+)/, function(x, y) {
                        return y.toLowerCase();
                    })
                    .replace(/^\\?([a-z]+\\[A-Z][a-z]+Bundle\\)/, function(x, y) {
                        var m = y.match(/^\\?([a-z]+\\)/);
                        return m[1];
                    })
                    .replace(/(\\Model\\)/, '\\')
                    .split(/\\/);
            if(parts.length>1 && parts[parts.length-1]===parts[parts.length-2]) {
                parts.pop();
            }
            tn = parts.join('');
            

        }
        tn = tn
                .replace(/(?:^|\.?)([A-Z])/g, function(x, y) {
                    return "_" + y.toLowerCase();
                }).replace(/^_/, "")
                .replace(/__/g, "_");
        let fields = [];

        for(let row of rows) {
            fields.push(row.asXml());
        }

        fields = fields.join("\n");
        this.node.innerText = `<?xml version="1.0" encoding="UTF-8"?>
<doctrine-mapping
    xmlns="http://doctrine-project.org/schemas/orm/doctrine-mapping"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://doctrine-project.org/schemas/orm/doctrine-mapping
    http://doctrine-project.org/schemas/orm/doctrine-mapping.xsd">
    <entity name="${className}" table="${tn}">
${fields}
    </entity>
</doctrine-mapping>`;
    }
}
