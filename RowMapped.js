/**
 * @author mati@we.ee
 * @type RowSubset
 */
class RowMapped extends RowSimple {

    constructor(...args) {
        super(... args);

    }

    phpConstruct() {
        var c = 'Doctrine\\Common\\Collections\\ArrayCollection';
        return `        $this->${this.property}s = new ${c}();`;
    }

    get interface() {
        return 'Doctrine\\Common\\Collections\\Collection';
    }

    phpPropery(pp, less_is_more) {
        var nameComm =  less_is_more? '': `\n     * ${this.commentName}`;
        return `    /**${nameComm}
     * @var ${this.type}[]|${this.interface} 
     */
    ${pp} $${this.property}s;`;
    }

    phpMethodGet(className, less_is_more) {
        var comment = '';
        if(!less_is_more) {
            comment = `    /**
     * Get ${this.commentName}s
     * @return ${this.type}[]|${this.interface}
     */
`;
        }
        return `${comment}    public function get${this.functionName}s(): ${this.interface}
    {
        return $this->${this.property}s;
    }`;

    }

    phpMethodSet(className, fluent, less_is_more) {
        var fn = this.functionName;
        var vn = fn.charAt(0).toLowerCase() + fn.substr(1);
        var mapped = this.mappedBy, mset = '';
        if (mapped) {
            let mv = (mapped.charAt(0).toUpperCase() + mapped.substr(1))
                .replace(/_([a-z])/g, function(g) {
                    return g[1].toUpperCase();
                }).replace(/_/g, "");
            mset = `        $${vn}->set${mv}($this);
`;
        }
        var rt = this.resolvedType;
        var ret = fluent? 'self': 'void';
        var retThis = fluent? "\n        return $this;": '';
        var retComm = fluent? `\n     * @return ${ret}`: '';

        var comment1 = '';
        var comment2 = '';
        if(!less_is_more) {
            comment1 = `    /**
     * Add ${this.commentName}
     * @param ${rt} $${vn}${retComm}
     */
`;
            comment2 = `    /**
     * Remove ${this.commentName}
     * @param ${this.resolvedType} $${vn}${retComm}
     */
`;
        }

        return `${comment1}    public function add${fn}(${rt} $${vn}): ${ret}
    {        
${mset}        $this->${this.property}s->add($${vn});${retThis}
    }

${comment2}    public function remove${fn}(${this.resolvedType} $${vn}): ${ret}
    {
        $this->${this.property}s->removeElement($${vn});${retThis}
    }`;
    }
}
