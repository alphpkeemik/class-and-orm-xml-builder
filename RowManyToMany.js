/**
 * @author mati@we.ee
 * @type RowManyToMany
 */
class RowManyToMany extends RowMapped {

    asXml() {
        return `        <many-to-many field="${this.property}s" target-entity="${this.resolvedType}" />`;
    }
}
