/**
 * @author mati@we.ee
 * @type RowManyToOne
 */
class RowManyToOne extends RowSimple {

    asXml() {
        return `        <many-to-one field="${this.property}"${this.databaseColumn} target-entity="${this.resolvedType}" />`;
    }

    get databaseColumn() {
        return '';
    }
}
