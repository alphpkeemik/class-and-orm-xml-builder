/**
 * @author mati@we.ee
 * @type ResolverSimple
 */
class ResolverSubset {
    execute(row) {
        let parts = row.split(' ');
        if (parts.length < 3) {
            return;
        }

        switch(true) {
            case parts[2] === 'mtm' && parts.length === 3:
                return new RowManyToMany(parts[0], parts[1]);
            case parts[2] === 'otm' && parts.length === 4:
                return new RowOneToMany(parts[3], parts[0], parts[1]);
            case parts[2] === 'mto' && parts.length === 3:
                return new RowManyToOne(parts[0], parts[1]);
            case parts[2] === 'oto' && parts.length === 3:
                return new RowOneToOne(parts[0], parts[1]);

        }
    }
}
