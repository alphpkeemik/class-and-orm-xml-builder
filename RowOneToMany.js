/**
 * @author mati@we.ee
 * @type RowOneToMany
 */
class RowOneToMany extends RowMapped {

    constructor(mappedBy, name, type) {
        super(name, type);
        this.mappedBy = mappedBy;

    }

    asXml() {
        return `        <one-to-many field="${this.property}s" target-entity="${this.resolvedType}" mapped-by="${this.mappedBy}" />`;
    }        
}
