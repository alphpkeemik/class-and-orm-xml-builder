/**
 * @author mati@we.ee
 * @type RowAi
 */
class RowAi extends RowSimple {

    phpMethodSet() {
        return '';
    }
    asXml() {

        return `        <id name="${this.property}" type="integer">
            <generator strategy="AUTO"/>
        </id>`;
    }
}