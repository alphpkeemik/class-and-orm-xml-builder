/**
 * @author mati@we.ee
 * @type RowSimple
 */
class Row {

    get commentName() {
        let name = this.name.replace(/_/g, ' ').replace(/(?:^|\.?)([A-Z])/g, function(x, y) {
            return " " + y.toLowerCase();
        }).replace(/^_/, "").replace(/  /g, " ").trim();

        return name.charAt(0).toUpperCase() + name.substr(1);
    }

    get resolvedType() {
        return this.type.replace(/\(\d+\)/, '')
            .replace(/^text$/, 'string')
            ;
    }

    get functionName() {
        let f = this.name.charAt(0).toUpperCase() + this.name.substr(1);
        return f.replace(/_([a-z])/g, function(g) {
            return g[1].toUpperCase();
        }).replace(/_/g, "");
    }

    get property() {
        return this.name.replace(/(?:^|\.?)([A-Z])/g, function(x, y) {
            return "_" + y.toLowerCase();
        }).replace(/^_/, "").replace(/__/g, "_");
    }

}
