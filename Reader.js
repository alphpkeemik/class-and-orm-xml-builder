/**
 * @author mati@we.ee
 * @type WeFormnewjavascriptclass
 */
class Reader {

    constructor() {
        this.resolvers = [];
    }
    addResolver(resolver) {
        this.resolvers.push(resolver);
    }
    read(raw) {
        var row;
        for(let resolver of this.resolvers) {
            row = resolver.execute(raw);
            if (row) {
                return row;
            }
        }
        throw new Error(`No resolver for row '${raw}`);
    }

}
