/**
 * @author mati@we.ee
 * @type Writer
 */
class Writer {
    constructor(node) {
        this.node = node;
    }

    clear() {
        this.node.innerHTML = '';
    }
}
