/**
 * @author mati@we.ee
 * @type WriterXmlname
 */
class WriterXmlname extends Writer {

    write(rows, className) {
        var tn = className;
        if (tn.indexOf('\\') >= 0) {
            let parts = tn
                    .replace(/\\?([A-Z]+)/, function(x, y) {
                        return y.toLowerCase();
                    })
                    .replace(/^\\?([a-z]+\\[A-Z][a-z]+Bundle\\)/, '')
                    .split(/\\/);
            if (parts.length > 1 && parts[parts.length - 1] === parts[parts.length - 2]) {
                parts.pop();
            }
            tn = parts.join('.');


        }


        this.node.innerText = tn + '.orm.xml';
    }
}
