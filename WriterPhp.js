/**
 * @author mati@we.ee
 * @type WriterPhp
 */
class WriterPhp extends Writer {

    constructor(...args) {
        super(...args);
        this.get = true;
        this.set = true;
        this.fluent_set = false;
        this.less_is_more = true;
        this.access = 'private';
        this.author = '';
    }

    write(rows, className) {
        var ccnp = className.split('\\');
        var cl = ccnp.pop();
        var cns = ccnp.join('\\');
        var html = '';


        for(let row of rows) {
            html += "\n";
            html += row.phpPropery(this.access, this.less_is_more);
            html += "\n";
        }
        html += "\n";
        var constructs = [];
        for(let row of rows) {
            if ('phpConstruct' in row) {
                constructs.push(row.phpConstruct());
            }
        }
        if (constructs.length) {
            html += '    public function __construct()' + "\n";
            html += '    {' + "\n";
            html += constructs.join("\n");
            html += "\n" + '    }' + "\n\n";
        }

        for(let row of rows) {
            if (this.get) {
                html += "\n";
                html += row.phpMethodGet(className, this.less_is_more);
                html += "\n";
            }
            if (this.set) {
                html += "\n";
                html += row.phpMethodSet(className, this.fluent_set, this.less_is_more);
                html += "\n";
            }
        }

        var ns = [], uns = [], uc = [], c, m, i, p, s, re = /[A-Z][a-z0-9A-Z]+(\\[A-Z][a-z0-9A-Z]+)+/g;

        while ((m = html.match(re))) {
            for(i = 0; i < m.length; i++) {
                if (uns.indexOf(m[i]) !== -1) {
                    continue;
                }
                uns.push(m[i]);
                p = m[i].split('\\');
                c = p.pop();
                if (p.join('\\') !== cns) {
                    s = m[i];
                    while (uc.indexOf(c) !== -1 && p.length) {
                        c += p.pop();
                        s = m[i] + ' as ' + c;
                    }
                    ns.push('use ' + s + ';');
                }
                uc.push(c);

                html = html.replace(new RegExp(m[i].replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), c)
            }
            break;
        }
        ns.sort();
        var nss = ns.join("\n");
        this.node.innerText = `<?php
namespace ${cns};

${nss}
/**
 * @author ${this.author}
 */

class ${cl} {
${html}
}
`;

    }
}
