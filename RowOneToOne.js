/**
 * @author mati@we.ee
 * @type RowOneToOne
 */
class RowOneToOne extends RowSimple {

    asXml() {
        return `        <one-to-one field="${this.property}"${this.databaseColumn} target-entity="${this.resolvedType}" />`;
    }

    get databaseColumn() {
        return '';
    }
}
