/**
 * @author mati@we.ee
 * @type RowSimple
 */
class RowSimple extends Row {

    constructor(name, type, isNull) {
        super();
        this.name = name;
        this.type = type;
        this.isNull = isNull;
    }

    phpPropery(pp, less_is_more) {
        var nameComm =  less_is_more? '': `\n     * ${this.commentName}`;
        return `    /**${nameComm}
     * @var ${this.phpType} 
     */
    ${pp} $${this.property};`;
    }

    phpMethodGet(className, less_is_more) {
        var ht = this.phpHintType;
        var returnType = '';


        if (ht) {
            returnType += `: `;
            if (this.isNull) {
                returnType += `?`;
            }
            returnType += `${ht}`;
        }
        var rth = this.phpType;
        if (this.isNull) {
            rth += '|null';
        }
        var comment = '';
        if(!less_is_more) {
            comment = `    /**
     * get ${this.commentName}
     * @return ${this.phpType}
     */
`;
        }
        return `${comment}    public function get${this.functionName}()${returnType}
    {
        return $this->${this.property};
    }`;
    }

    phpMethodSet(className, fluent, less_is_more) {
        var fn = this.functionName;
        var vn = fn.charAt(0).toLowerCase() + fn.substr(1);
        var arg = `${this.phpHintType} $${vn}`.trim();

        if (this.isNull) {
            arg += ' = null';
        }
        var ret = fluent? 'self': 'void';
        var retThis = fluent? "\n        return $this;": '';
        var retComm = fluent? `\n     * @return ${ret}`: '';
        var comment = '';
        if(!less_is_more) {
            comment = `    /**
     * get ${this.commentName}
     * @param ${this.phpType} $${vn}${retComm}
     */
`;
        }
        return `${comment}    public function set${fn}(${arg}): ${ret}
    {
        $this->${this.property} = $${vn};${retThis}
    }`;
    }

    get phpType() {
        var rt = this.resolvedType;
        if (rt === 'smallint' || rt === 'bigint') {
            rt = 'int';
        }
        if (rt === 'time' || rt === 'datetime' || rt === 'date') {
            rt = '\DateTime';
        }
        if (rt.match(/(?:(?:decimal)|(?:double))\((\d+),(\d+)\)/)) {
            rt = 'float';
        }
        return rt;
    }

    get phpHintType() {
        var rt = this.phpType;
        if (['mixed', 'object'].indexOf(rt) > -1) {
            return '';
        }
        return rt;
    }

    asXml() {
        var matches = this.type.match(/\((\d+)\)/);
        var extra = '', rt = this.resolvedType;
        if (matches) {
            extra += ` length="${matches[1]}"`;
        } else if (rt === 'string') {
            rt = 'text';
        }
        if (rt === 'int') {
            rt = 'integer';
        }
        if (rt === 'bool') {
            rt = 'boolean';
        }
        if (rt === 'mixed') {
            rt = 'json';
        }
        if (rt.match(/^[A-Z][a-z]+/) || rt.match(/\\/)) {
            rt = 'object';
        }
        if (this.isNull) {
            extra += ' nullable="true"';
        }
        matches = this.type.match(/(?:(?:decimal)|(?:double))\((\d+),(\d+)\)/);
        if (matches) {
            rt = 'decimal';
            extra += ` precision="${matches[1]}"`;
            extra += ` scale="${matches[2]}"`;
        }


        return `        <field name="${this.property}"${this.databaseColumn} type="${rt}"${extra} />`;
    }

    get databaseColumn() {
        if ([
                'ACCESSIBLE', 'ADD', 'ALL', 'ALTER', 'ANALYZE', 'AND', 'AS',
                'ASC', 'ASENSITIVE', 'BEFORE', 'BETWEEN', 'BIGINT', 'BINARY',
                'BLOB', 'BOTH', 'BY', 'CALL', 'CASCADE', 'CASE', 'CHANGE', 'CHAR',
                'CHARACTER', 'CHECK', 'COLLATE', 'COLUMN', 'CONDITION',
                'CONSTRAINT', 'CONTINUE', 'CONVERT', 'CREATE', 'CROSS',
                'CURRENT_DATE', 'CURRENT_TIME', 'CURRENT_TIMESTAMP',
                'CURRENT_USER', 'CURSOR', 'DATABASE', 'DATABASES', 'DAY_HOUR',
                'DAY_MICROSECOND', 'DAY_MINUTE', 'DAY_SECOND', 'DEC', 'DECIMAL',
                'DECLARE', 'DEFAULT', 'DELAYED', 'DELETE', 'DESC', 'DESCRIBE',
                'DETERMINISTIC', 'DISTINCT', 'DISTINCTROW', 'DIV', 'DOUBLE',
                'DROP', 'DUAL', 'EACH', 'ELSE', 'ELSEIF', 'ENCLOSED', 'ESCAPED',
                'EXISTS', 'EXIT', 'EXPLAIN', 'FALSE', 'FETCH', 'FLOAT', 'FLOAT4',
                'FLOAT8', 'FOR', 'FORCE', 'FOREIGN', 'FROM', 'FULLTEXT',
                'GENERATED', 'GET', 'GRANT', 'GROUP', 'HAVING', 'HIGH_PRIORITY',
                'HOUR_MICROSECOND', 'HOUR_MINUTE', 'HOUR_SECOND', 'IF', 'IGNORE',
                'IN', 'INDEX', 'INFILE', 'INNER', 'INOUT', 'INSENSITIVE',
                'INSERT', 'INT', 'INT1', 'INT2', 'INT3', 'INT4', 'INT8',
                'INTEGER', 'INTERVAL', 'INTO', 'IO_AFTER_GTIDS',
                'IO_BEFORE_GTIDS', 'IS', 'ITERATE', 'JOIN', 'KEY', 'KEYS', 'KILL',
                'LEADING', 'LEAVE', 'LEFT', 'LIKE', 'LIMIT', 'LINEAR', 'LINES',
                'LOAD', 'LOCALTIME', 'LOCALTIMESTAMP', 'LOCK', 'LONG', 'LONGBLOB',
                'LONGTEXT', 'LOOP', 'LOW_PRIORITY', 'MASTER_BIND',
                'MASTER_SSL_VERIFY_SERVER_CERT', 'MATCH', 'MAXVALUE',
                'MEDIUMBLOB', 'MEDIUMINT', 'MEDIUMTEXT', 'MIDDLEINT',
                'MINUTE_MICROSECOND', 'MINUTE_SECOND', 'MOD', 'MODIFIES',
                'NATURAL', 'NO_WRITE_TO_BINLOG', 'NOT', 'NULL', 'NUMERIC', 'ON',
                'OPTIMIZE', 'OPTIMIZER_COSTS', 'OPTION', 'OPTIONALLY', 'OR',
                'ORDER', 'OUT', 'OUTER', 'OUTFILE', 'PARTITION', 'PRECISION',
                'PRIMARY', 'PROCEDURE', 'PURGE', 'RANGE', 'READ', 'READ_WRITE',
                'READS', 'REAL', 'REFERENCES', 'REGEXP', 'RELEASE', 'RENAME',
                'REPEAT', 'REPLACE', 'REQUIRE', 'RESIGNAL', 'RESTRICT', 'RETURN',
                'REVOKE', 'RIGHT', 'RLIKE', 'SCHEMA', 'SCHEMAS',
                'SECOND_MICROSECOND', 'SELECT', 'SENSITIVE', 'SEPARATOR', 'SET',
                'SHOW', 'SIGNAL', 'SMALLINT', 'SPATIAL', 'SPECIFIC', 'SQL',
                'SQL_BIG_RESULT', 'SQL_CALC_FOUND_ROWS', 'SQL_SMALL_RESULT',
                'SQLEXCEPTION', 'SQLSTATE', 'SQLWARNING', 'SSL', 'STARTING',
                'STORED', 'STRAIGHT_JOIN', 'TABLE', 'TERMINATED', 'THEN',
                'TINYBLOB', 'TINYINT', 'TINYTEXT', 'TO', 'TRAILING', 'TRIGGER',
                'TRUE', 'UNDO', 'UNION', 'UNIQUE', 'UNLOCK', 'UNSIGNED', 'UPDATE',
                'USAGE', 'USE', 'USING', 'UTC_DATE', 'UTC_TIME', 'UTC_TIMESTAMP',
                'VALUES', 'VARBINARY', 'VARCHAR', 'VARCHARACTER', 'VARYING',
                'VIRTUAL', 'WHEN', 'WHERE', 'WHILE', 'WITH', 'WRITE', 'XOR',
                'YEAR_MONTH', 'ZEROFILL', 'CONNECTION', 'GENERAL', 'GOTO',
                'IGNORE_SERVER_IDS', 'LABEL', 'MASTER_HEARTBEAT_PERIOD', 'RAID0',
                'RECURSIVE', 'ROWS', 'SLOW', 'SONAME', 'X509', 'ANALYSE', 'ANY',
                'ARRAY', 'ASYMMETRIC', 'AUTHORIZATION', 'CAST', 'CONCURRENTLY',
                'CURRENT_CATALOG', 'CURRENT_ROLE', 'CURRENT_SCHEMA', 'DEFERRABLE',
                'DO', 'END', 'EXCEPT', 'FREEZE', 'FULL', 'ILIKE', 'INITIALLY',
                'INTERSECT', 'ISNULL', 'NOTNULL', 'OFFSET', 'ONLY', 'OVERLAPS',
                'PLACING', 'RETURNING', 'SESSION_USER', 'SIMILAR', 'SOME',
                'SYMMETRIC', 'USER', 'VARIADIC', 'VERBOSE', 'WINDOW', 'COLLATION',
                'LATERAL', 'OVER', 'NEW', 'OFF', 'OLD'
            ].indexOf(this.property.toUpperCase()) > -1) {
            return ' column="`' + this.property + '`"';
        }
        return '';
    }
}
