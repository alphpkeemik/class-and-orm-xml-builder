/**
 * @author mati@we.ee
 * @type Handler
 */
class Handler {

    constructor(contentNode, classNode, reader, errorWriter) {
        this.contentNode = contentNode;
        this.classNode = classNode;
        this.reader = reader;
        this.errorWriter = errorWriter;
        this.writers = [];
    }
    addWriter(writer) {
        this.writers.push(writer);
    }
    startListening() {
        for(let event of 'blur change paste update keyup click'.split(' ')) {
            this.contentNode.addEventListener(event, this.read.bind(this), {passive: true});
            this.classNode.addEventListener(event, this.read.bind(this), {passive: true});
        }
    }
    read() {
        for(let writer of this.writers) {
            writer.clear();
        }
        this.errorWriter.clear();
        var rows = [];
        try {
            for(let raw of this.contentNode.value.trim().replace(/\t/g, ' ').split("\n")) {
                raw = raw.trim();
                if (!raw) {
                    continue;
                }
                rows.push(this.reader.read(raw));
            }

            for(let writer of this.writers) {
                writer.write(rows, this.classNode.value);
            }
        } catch(e) {
            this.errorWriter.write(e);
            console.debug(e);
        }
    }
}

