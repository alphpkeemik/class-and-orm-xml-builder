/**
 * @author mati@we.ee
 * @type ResolverSimple
 */
class ResolverSimple {
    execute(row) {
        let parts = row.split(' ');
        if (parts.length > 3) {
            return;
        }
        if (parts.length === 3 && parts[2] !== 'null') {
            return;
        }
        return new RowSimple(parts[0], parts[1] || 'mixed', parts.length === 3 ? true : false);
    }
}
